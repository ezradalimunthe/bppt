<?php
use app\components\DashboardInfoBox;
use app\components\Breadcrumb;
use app\assets\ChartjsAsset;
ChartjsAsset::register($this);

$this->registerJsFile(
  '@web/js/site/index.js',
  ['depends' => [\app\assets\AppAsset::className()]]
);
?>
<?=Breadcrumb::widget(['title'=>'Dashboard', 'icon'=>'fa fa-bar-chart'])?>
<div class="row">
<?=DashboardInfoBox::widget(
    ['title' => 'Nilai Asset', 
    'description' => 'Rp. 17.254.333.254,-',
    'icon'=>'fa-money'
    ])?>

<?=DashboardInfoBox::widget(
    ['title' => 'Tanah', 
    'description' => '254 ha',
    'icon'=>'fa-map',
    'boxColour'=>'danger'
    ])?>
<?=DashboardInfoBox::widget(
    ['title' => 'Bangunan', 
    'description' => '17 Unit',
    'icon'=>'fa-building',
    'boxColour'=>'info'
    ])?>
<?=DashboardInfoBox::widget(
    ['title' => 'Lokasi Barang', 
    'description' => '<a href="/site/track">Pencarian</a>',
    'icon'=>'fa-building',
    'boxColour'=>'info'
    ])?>
</div>
       
<div class="row">
<div class="col-sm-12 col-lg-6">
  <div class="tile">
    <h3 class="tile-title">Usulan Pengadaan Barang</h3>
    <div class="embed-responsive embed-responsive-16by9">
      <canvas class="embed-responsive-item" id="chartrequeststate"></canvas>
    </div>
  </div>
</div>
<div class="col-sm-12 col-lg-6">
  <div class="tile">
    <h3 class="tile-title">Kondisi BMN</h3>
    <div class="embed-responsive embed-responsive-16by9">
      <canvas class="embed-responsive-item" id="chartassetcondition"></canvas>
    </div>
  </div>
  </div>
</div>
</div>