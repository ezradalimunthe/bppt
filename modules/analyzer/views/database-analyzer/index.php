<?php
use app\components\Breadcrumb;
use app\modules\analyzer\assets\AnalyzerAsset;
use yii\data\ArrayDataProvider;
AnalyzerAsset::register($this);
$this->title = "Database Analyzer";

$tables = new ArrayDataProvider(['allModels' => $connectiondata->tables,
    'pagination' => false]);

 $uname =     php_uname();
 $os = PHP_OS;
?>
<?=Breadcrumb::widget(['title' => $this->title, 'icon' => 'fa fa-bar-chart'])?>
<style>


</style>

<div class="row">
    <div class="col-sm-12">
        <div class="tile">
            <h3 class="tile-title">Database Info</h3>
            <div class="container-fluid">
                <dl class="row">
                <dt class="col-sm-2">Host</dt>
                <dd class="col-sm-10">: <?=$connectiondata->hostname?></dd>
                <dt class="col-sm-2">Mysql Version</dt>
                <dd class="col-sm-10">: <?=$connectiondata->version?></dd>
                <dt class="col-sm-2">Databasename</dt>
                <dd class="col-sm-10">: <?=$connectiondata->database?></dd>
                <dt class="col-sm-2">Unix Name</dt>
                <dd class="col-sm-10">: <?=$uname?></dd>
                <dt class="col-sm-2">Operating System</dt>
                <dd class="col-sm-10">: <?=$os?></dd>
                <dt class="col-sm-2">Innodb Status</dt>
                <dd class="col-sm-10">: <button id="btnInnodbStatusShow" class="btn btn-outline-primary btn-sm">Show</button></dd>
                </dl>
            </div>
            <!--tables-->
            <h3 class="tile-title">Tables</h3>
            <div class="container-fluid mb-4">
              <div class="row">
                <div class="col-sm-4">
                    <div class="tile">
                    <div class="tile-title">
                        <h3><small>Number Of Tables </small> <?=sizeof($connectiondata->tables);?></h3>
                    </div>
                        <div class="tile-body">

                            <div class='list-group mt-1 border border-light database-table-list' >
                                <?php
for ($i = 0; $i < sizeof($connectiondata->tables); $i++) {
    echo "<a data-tablename='" . $connectiondata->tables[$i] . "' href='#' class='list-group-item list-group-item-action database-item-list-table-action'> " .
    $connectiondata->tables[$i] .
        "</a>\n";
}
?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="tile">
                        <div class="tile-title">
                        <h3><small>Definition of</small> <span id="table_name"></span></h3>
                        </div>
                        <div clas="tile-body">
                            <dl class="row">
                                <dt class="col-sm-3">Engine</dt>
                                <dd class="col-sm-9">: <span id="table_engine"></span>&nbsp;</dd>
                                <dt class="col-sm-3">Data Length</dt>
                                <dd class="col-sm-9">: <span id="table_data_length"></span>&nbsp;</dd>
                                <dt class="col-sm-3">Rows Count</dt>
                                <dd class="col-sm-9">: <span id="table_rows_count"></span>&nbsp;</dd>
                                <dt class="col-sm-3">Create Time</dt>
                                <dd class="col-sm-9">: <span id="table_create_time"></span>&nbsp;</dd>
                                <dt class="col-sm-3">Update Time</dt>
                                <dd class="col-sm-9">: <span id="table_update_time"></span>&nbsp;</dd>
                            </dl>
                            <div >
                                <table class='table table-bordered' id="database-table">
                                    <thead>
                                        <tr>
                                        <th class="col-sm-3">Field</th>
                                            <th class="col-sm-3">Type</th>
                                            <th class="col-sm-6">Other</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <!--tables end-->
            <!--views -->
            <h3 class="tile-title">Views</h3>
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-4">
                    <div class="tile">
                        <div class="tile-body">
                            <div class="title">Number Of Views: <?=sizeof($connectiondata->views);?></div>
                            <div class='list-group mt-1 border border-light database-table-list' >
                                <?php
for ($i = 0; $i < sizeof($connectiondata->views); $i++) {
    echo "<a data-tablename='" . $connectiondata->views[$i] . "' href='#' class='list-group-item list-group-item-action database-item-list-view-action'> " .
    $connectiondata->views[$i] .
        "</a>";
}
?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="card">
                    <div class="card-header">
                        <h3><small>Definition of</small> <span id="view_name"></span></h3>
                        </div>
                            <div clas="card-body">
                                <div>
                                    <table class='table table-bordered' id="database-view">
                                        <thead>
                                            <tr>
                                            <th class="col-sm-3">Field</th>
                                            <th class="col-sm-3">Type</th>
                                            <th class="col-sm-6">Other</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <!--views end-->
        </div>
    </div>
</div>
<!-- modal dialog-->
<div class="modal" id="innodbstatus_modal" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Innodb Status</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="tile">
                    <div class="overlay" id="innodbstatus_overlay">
                        <div class="m-loader mr-4">
                            <svg class="m-circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
                            </svg>
                        </div>
                        <h3 class="l-text">Loading</h3>

                    </div>
                    <pre id="innodbstatus_text">

                    </pre>

                </div>
                
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--end modal dialog-->