<?php
use app\components\Breadcrumb;
use yii\helpers\Html;

$this->title= "System Analyzer"
?>
<?=Breadcrumb::widget(['title' => 'System Analyzer', 'icon' => 'fa fa-bar-chart'])?>

<div class="list-group">
  <?=Html::a("Page Analyzer", "page-analyzer", ["class"=>"list-group-item list-group-item-action"]);?>
  <?=Html::a("Database Analyzer", "database-analyzer", ["class"=>"list-group-item list-group-item-action"]);?>
  
</div>