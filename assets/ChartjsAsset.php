<?php
namespace app\assets;

use yii\web\AssetBundle;

class ChartjsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/nnnick/chartjs/dist/';
    public function init()
    {
        $this->js = YII_DEBUG ? ['Chart.js'] : ['Chart.min.js'];
        $this->css = YII_DEBUG ? ['Chart.css'] : ['Chart.min.css'];
    }
    public $depends = [
            'app\assets\AppAsset'
            ]; 
}

