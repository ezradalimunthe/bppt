<?php
namespace app\assets;

use yii\web\AssetBundle;

class DatatableNetAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public function init()
    {
        $this->js = YII_DEBUG ? 
            ['datatables.net/js/jquery.dataTables.js',
             'datatables.net-bs4/js/dataTables.bootstrap4.js'] : 
            ['datatables.net/js/jquery.dataTables.min.js',
            'datatables.net-bs4/js/dataTables.bootstrap4.min.js'];
        $this->css = YII_DEBUG ? ['datatables.net-bs4/css/dataTables.bootstrap4.css'] :
                    ['datatables.net-bs4/css/dataTables.bootstrap4.min.css'] ;
    }
    public $depends = [
            'app\assets\AppAsset'
            ]; 
}

