<?php

namespace app\extension\openlayer;

use yii\web\AssetBundle;

class OpenlayerAsset extends AssetBundle
{
    public $sourcePath = __DIR__;
    public function init()
    {
        $this->js = ['https://openlayers.org/en/v4.6.5/build/ol.js'];
        $this->css = ['https://openlayers.org/en/v4.6.5/css/ol.css'];
    }

    public $depends = [
        'app\assets\AppAsset',
    ];
}
