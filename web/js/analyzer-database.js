var database_table = null;
var database_view = null;
$(function () {
    database_table = $('#database-table').DataTable(featuredef);
    database_view = $('#database-view').DataTable(featuredef);
    bindTableListAction();
    $("#btnInnodbStatusShow").click(showInnodbStatus)

})

showInnodbStatus=function(e){
    $("#innodbstatus_modal").modal();
    $.ajax({
        url: "database-analyzer/innodbstatus",
        method: "POST",
        dataType: "json",
        success: function (result) {
            $("#innodbstatus_overlay").hide();
            $("#innodbstatus_text").text(result);
        }

    })
}

render_info = function (data, type, row, meta) {
    if (type === 'display') {
        var html = " <dl class='row'>";
        html += "<dt class='col-md-4'>Key: </dt><dd class='col-md-8'>" + row.Key + "&nbsp;</dd>";
        html += "<dt class='col-md-4'>Nullable: </dt><dd class='col-md-8'>" + data + "&nbsp;</dd>";
        html += "<dt class='col-md-4'>Default: </dt><dd class='col-md-8'>" + row.Default + "&nbsp;</dd>";
        html += "<dt class='col-md-4'>Extra: </dt><dd class='col-md-8'>" + row.Extra + "&nbsp;</dd>";
        html += "</dl>";
        return html;
    }
    return data;

}
featuredef = {
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    columns: [
        { name: "Field", data: "Field" },
        { name: "Type", data: "Type" },
        { name: "Null", data: "Null", render: render_info }

    ],

}


bindTableListAction = function () {
    $(".database-item-list-view-action").click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $("#view_name").text($(this).data("tablename"));
        showTableDef($(this).data("tablename"), database_view)

    })
    $(".database-item-list-table-action").click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $("#table_name").text($(this).data("tablename"));
        showTableDef($(this).data("tablename"), database_table)

    })
}
showTableDef = function (objectName, target) {
    target.clear();

    $.ajax({
        url: "database-analyzer/object-def",
        data: { "objectname": objectName },
        method: "POST",
        dataType: "json",
        success: function (r) {
            target.rows.add(r.coldefs).draw();
            var tabledefs = r.tabledefs;
            if (tabledefs.type=="BASE TABLE"){
                $("#table_engine").html(tabledefs.engine);
                $("#table_data_length").html(tabledefs.datalength);
                $("#table_rows_count").html(tabledefs.rowscount);
                $("#table_create_time").html(tabledefs.createdtime);
                $("#table_update_time").html(tabledefs.updatetime);
            }
            

        }

    })
}

