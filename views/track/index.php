<?php
use app\components\Breadcrumb;
use app\extension\openlayer\OpenlayerAsset;

OpenlayerAsset::register($this);

$this->registerCssFile(
    '@web/css/track.css',
    ['depends' => [\app\assets\AppAsset::className()]]
  );

$this->registerJsFile(
    '@web/js/track/index.js',
    ['depends' => [\app\assets\AppAsset::className()]]
  );
?>
<?=Breadcrumb::widget(['title' => 'Pencarian', 'icon' => 'fa fa-bar-chart'])?>


<div class="row">
   <div class="col-md-12">
      <div class="tile">
         <div class="row">
            <div class="col-md-8">
                <div style="position:relative;height:500px">
                    <div id="map" style="height:100%;">
                    </div>
                </div>
                
            </div>
                    
            <div class="col-md-4">
                <div class="input-group">
                    <input class="form-control" id="search_mobile_asset" type="text" placeholder="Plat / Kode barang"/>
                    <div class="input-group-append">
                        <button class="app-search__button"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <ul class="list-group list-group-flush" id='mobile_search_result'>
                    <li class="list-group-item"><i class="fa fa-car"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-laptop"></i> Laptop 101</li>
                    <li class="list-group-item"><i class="fa fa-car"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-motorcycle"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-laptop"></i> Laptop 102</li>
                    <li class="list-group-item"><i class="fa fa-bus"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-truck"></i> B 1234 XXX</li>
                    
                </ul>
            </div>
            </div>
         
      </div>
   </div>
</div>



