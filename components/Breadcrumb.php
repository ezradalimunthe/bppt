<?php
namespace app\components;

use yii\base\Widget;

class Breadcrumb extends Widget
{
    public $icon = "fa fa-dashboard";
    public $title="title";
    public $subtitle = "SIM Layanan BMN Plus";
    public $breadcrumb=[];
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        $result = '<div class="app-title">' .
            '<div>' .
            '<h1><i class="'.$this->icon.'"></i> '.$this->title.'</h1>' ;
            if ($this->subtitle!=""){
               $result .= '<p>'.$this->subtitle.'</p>'; 
            }
            
        $result.= '</div>' .
            '<ul class="app-breadcrumb breadcrumb">' .
            '<li class="breadcrumb-item"><a href="/"><i class="fa fa-home fa-lg"></i></a></li>' ;
            if (sizeof($this->breadcrumb)>0){
                $result .='<li class="breadcrumb-item"><a href="#">'.$this->title.'</a></li>';
            }
            
        $result.=     '</ul>' .
                '</div>';
        return $result;

    }
}


/* <div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> <?=$this->title?></h1>
    <p>Start a beautiful journey here</p>
   </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="#"><?=$this->title?></a></li>
  </ul>
</div> */