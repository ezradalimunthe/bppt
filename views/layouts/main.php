<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* layout base on http://pratikborsadiya.in/vali-admin/index.html */
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>

  <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
  <?php $this->beginBody() ?>
  <?php echo $this->render('topmenu'); ?>
  <?php echo $this->render('sidebarmenu' ); ?>
    <main class="app-content">
    <?= $content ?>
    <div class="row">
     <div class="col-md-12">
     <div style="height:30px;background-color:#fff;padding:5px 5px" class='text-right'> SIM Layanan BMN Plus</div>
     </div>
      
    </div>
    </main>
   
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
