<?php

namespace app\modules\analyzer\controllers;

use yii\web\Controller;
use yii\db\Query;
/**
 * Default controller for the `analyzer` module
 */
class DatabaseAnalyzerController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $connection = \Yii::$app->db;
        $connectiondata = new \stdClass;
        $connectiondata->dsn = $connection->dsn;

        //$cmd  = $connection->createCommand("SHOW FULL TABLES IN smile_x");
        //
        $connectiondata->hostname = $connection->createCommand("SELECT @@HOSTNAME;")->
            queryAll()[0]["@@HOSTNAME"];

        $connectiondata->version = $connection->createCommand("SELECT @@VERSION;")->
            queryAll()[0]["@@VERSION"];
        $connectiondata->database = $connection->createCommand("SELECT DATABASE();")->
            queryAll()[0]["DATABASE()"];
     
      
        $allobjects = $connection->createCommand("SHOW FULL TABLES")->
            queryAll();
        $connectiondata->tables = [];
        $connectiondata->views = [];
        foreach ($allobjects as $k => $v) {
            switch ($v["Table_type"]) {
                case "BASE TABLE":
                    $connectiondata->tables[] = $v["Tables_in_" . $connectiondata->database];
                    break;
                case "VIEW":
                    $connectiondata->views[] = $v["Tables_in_" . $connectiondata->database];
                    break;

            }
        }
        return $this->render('index', ["connectiondata"=>$connectiondata]);
    }
/**
 * get the definition of table/ view in database
 * return ajax json value.
 */
    public function actionObjectDef(){
        $formatter = \Yii::$app->formatter;
        $formatter->locale='id-ID';
        $objectName = \Yii::$app->request->post("objectname");
        $connection =  \Yii::$app->db;
        $dbname = $connection->createCommand("SELECT DATABASE();")->
                    queryAll()[0]["DATABASE()"];
        $coldefs = (new Query)
            ->select (["Field"=>"COLUMN_NAME", 
                       "Type"=>"COLUMN_TYPE" ,
                       "Null"=>"IS_NULLABLE",
                       "Key"=>"COLUMN_KEY",
                       "Default"=>"COLUMN_DEFAULT",
                       "Extra"=>"EXTRA"
                       ])
            ->from ("INFORMATION_SCHEMA.COLUMNS")
            ->where ("table_name = :tablename", array(":tablename"=>$objectName))
            ->andWhere("table_schema = :dbname", array(":dbname"=>$dbname))
            ->all();

        $tabledefs = (new Query)
            ->select (["name"=>"TABLE_NAME", 
                       "type"=>"TABLE_TYPE",
                       "engine"=>"ENGINE",
                       "datalength"=>"DATA_LENGTH",
                       "rowscount"=>"TABLE_ROWS",
                       "createdtime"=>"CREATE_TIME",
                       "updatetime"=>"UPDATE_TIME"
                       ])
            ->from ("INFORMATION_SCHEMA.TABLES")
            ->where ("table_name = :tablename", array(":tablename"=>$objectName))
            ->andWhere("table_schema = :dbname", array(":dbname"=>$dbname))
            ->all();

            $returnvalue = new \stdClass;
            $returnvalue->coldefs = $coldefs;
            $returnvalue->tabledefs = new \stdClass;
            $returnvalue->tabledefs->engine = $tabledefs[0]["engine"];
            $returnvalue->tabledefs->type = $tabledefs[0]["type"];
            $returnvalue->tabledefs->datalength = $formatter->asInteger( $tabledefs[0]["datalength"] ) ." bytes";
            $rcount = $tabledefs[0]["rowscount"];
            $rcountunit = $rcount==0? "row":"rows";
            $returnvalue->tabledefs->rowscount = $formatter->asInteger($rcount) . " " . $rcountunit;
            $returnvalue->tabledefs->createdtime = $formatter->asDatetime($tabledefs[0]["createdtime"]);
            $returnvalue->tabledefs->updatetime = $formatter->asDatetime($tabledefs[0]["updatetime"]);

            $returnvalue->objectname= $objectName;

          return $this->asJson($returnvalue) ;

    }
    /**
     * get innodb status
     */
    public function actionInnodbstatus(){
        $connection = \Yii::$app->db;
        $result = $connection->createCommand("SHOW ENGINE INNODB STATUS")->
        queryAll()[0]["Status"];
        return $this->asJson($result);
    }

}



  
