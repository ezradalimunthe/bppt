<?php
namespace app\modules\analyzer\assets;

use yii\web\AssetBundle;   

class AnalyzerAsset extends AssetBundle {

    // The directory that contains the source asset files for this asset bundle
    //public $sourcePath = '@app/modules/analyzer/web/';
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    // List of CSS files that this bundle contains
    public $css = ['css/analyzer.css'];
    public $js = ['js/analyzer.js','js/analyzer-database.js'];
    public $depends = [
        'app\assets\AppAsset',
        'app\assets\DatatableNetAsset'
        ]; 

}